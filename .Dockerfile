
ARG PKG_REGISTRY
ARG PKG_REGISTRY_LOGIN
ARG PKG_REGISTRY_PASS

FROM golang:1.16.6-alpine as scratch
RUN go env -w GO111MODULE=on
RUN go env -w GOPRIVATE=$PKG_REGISTRY
RUN echo "machine $PKG_REGISTRY login $PKG_REGISTRY_LOGIN password $PKG_REGISTRY_PASS" > $HOME/.netrc

FROM scratch as dev

FROM scratch AS build
WORKDIR /project
COPY . .
ENV ENV=prod
RUN GOOS=linux GARCH=amd64 CGO_ENABLED=0 go build -o front-gateway .
RUN chmod +x front-gateway

FROM scratch as prod
COPY --from=build /project/front-gateway .
RUN ln -s /project/front-gateway /usr/bin/front-gateway
CMD ["front-gateway"]
