# front-gateway

Commands:
$ `openapi-generator-cli generate -i {{.../openapi.yml}} -g go-server -o ./openapi`

Mocks:
$ `mockgen -package=mocks gitlab.com/supply-demand/proto_gen/go/user UserServiceClient > ./tests/mocks/grpc_user_service.go`
$ `mockgen -package=mocks -source=./dependency/session/storage.go Storage > ./tests/mocks/session_storage.go`
$ `mockgen -package=mocks -source=./dependency/clock/clock.go Clock > ./tests/mocks/clock.go`
