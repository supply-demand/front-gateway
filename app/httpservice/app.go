package httpservice

import (
	"context"
	"github.com/sirupsen/logrus"
	"gitlab.com/supply-demand/auth/app/httpservice/handlers"
	"gitlab.com/supply-demand/auth/app/httpservice/middlewares"
	"gitlab.com/supply-demand/auth/app/httpservice/services"
	"gitlab.com/supply-demand/auth/config"
	"gitlab.com/supply-demand/auth/dependency/clock"
	"gitlab.com/supply-demand/auth/dependency/opentracing"
	"gitlab.com/supply-demand/auth/dependency/redis"
	"gitlab.com/supply-demand/auth/dependency/session"
	"go.uber.org/fx"
)

func registerModule(lc fx.Lifecycle, manager *ServerManager) {
	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			return manager.Start()
		},
		OnStop: func(ctx context.Context) error {
			return manager.Stop()
		},
	})
}

func Run(ctx context.Context) error {
	appConfig, err := config.NewConfig()
	if err != nil {
		return err
	}

	logger := logrus.New()
	logger.SetFormatter(&logrus.JSONFormatter{})

	var (
		errCh  chan error
		stopCh chan bool
	)

	opts := []fx.Option{
		fx.Supply(appConfig),
		fx.Supply(logger),
		fx.Supply(errCh),
		redis.Module,
		session.Module,
		opentracing.Module,
		fx.Provide(
			clock.NewClock,
			NewConfig,
			NewFastHttpHandler,
			NewFastHttpServer,
			NewErrorHandler,
			NewServerManager,
			services.NewAuthService,
			middlewares.NewLoggerMiddleware,
			middlewares.NewTracingMiddleware,
			handlers.NewAuthHandler,
		),
		fx.Invoke(
			registerModule,
		),
	}

	app := fx.New(opts...)

	go func() {
		for {
			select {
			case err := <-errCh:
				logger.WithField("err", err).Info("Error propagated to the top")
				stopCh <- true
			case <-stopCh:
				if err := app.Stop(ctx); err != nil {
					logger.WithField("error", err).Info("Could not stop http service module gracefully")
				}
				break
			}
		}
	}()

	app.Run()

	return app.Err()
}
