package httpservice

import (
	"github.com/Netflix/go-env"
)

type Config struct {
	ApiPort int    `env:"API_PORT,default=8010"`
	ApiAddr string `env:"API_ADDR,default=127.0.0.1"`
}

func NewConfig() (*Config, error) {
	cfg := Config{}

	_, err := env.UnmarshalFromEnviron(&cfg)

	if err != nil {
		return nil, err
	}

	return &cfg, err
}
