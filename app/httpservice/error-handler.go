package httpservice

import (
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/supply-demand/auth/app/httpservice/services"

	openapi "gitlab.com/supply-demand/auth/openapi/go"
)

func NewErrorHandler(l *logrus.Logger) openapi.ErrorHandler {
	return func(w http.ResponseWriter, r *http.Request, err error, result *openapi.ImplResponse) {
		var (
			body interface{}
			code int
		)

		if _, ok := err.(*services.ErrUnauthorized); ok {
			code = http.StatusUnauthorized
			body = openapi.Unauthorized{}
		} else {
			l.
				WithField("code", result.Code).
				WithField("body", result.Body).
				WithError(err).
				Error("unhandled error")

			code = http.StatusInternalServerError
			body = map[string]string{
				"message": "Something went wrong",
			}
		}

		errEncoding := openapi.EncodeJSONResponse(body, &code, w)
		if errEncoding != nil {
			l.WithError(errEncoding).Info("could not encode json response")
		}
	}
}
