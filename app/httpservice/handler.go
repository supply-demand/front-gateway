package httpservice

import (
	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/fasthttpadaptor"

	openapi "gitlab.com/supply-demand/auth/openapi/go"
)

func NewFastHttpHandler(
	errorHandler openapi.ErrorHandler,
	authHandler openapi.AuthApiServicer,
) fasthttp.RequestHandler {
	r := router.New()

	r.GET("/healthy", func(ctx *fasthttp.RequestCtx) {
		ctx.SetStatusCode(200)
	})

	r.GET("/ready", func(ctx *fasthttp.RequestCtx) {
		ctx.SetStatusCode(200)
	})

	authController := openapi.NewAuthApiController(authHandler, openapi.WithAuthApiErrorHandler(errorHandler))
	routes := authController.Routes()

	for _, route := range routes {
		handler := fasthttpadaptor.NewFastHTTPHandlerFunc(route.HandlerFunc)
		r.Handle(route.Method, route.Pattern, handler)
	}

	return r.Handler
}
