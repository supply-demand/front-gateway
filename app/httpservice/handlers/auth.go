package handlers

import (
	"context"
	"net/http"

	"gitlab.com/supply-demand/auth/app/httpservice/services"
	"gitlab.com/supply-demand/auth/domain/auth"

	openapi "gitlab.com/supply-demand/auth/openapi/go"
)

var dummyResponse = openapi.ImplResponse{}

type AuthHandler struct {
	signUpService  *auth.SignUpService
	signInService  *auth.SignInService
	confirmService *auth.ConfirmService
	prolongService *auth.ProlongSessionService
	authService    *services.AuthService
}

func NewAuthHandler(
	signUpService *auth.SignUpService,
	signInService *auth.SignInService,
	confirmService *auth.ConfirmService,
	prolongService *auth.ProlongSessionService,
	authService *services.AuthService,
) openapi.AuthApiServicer {
	return &AuthHandler{
		signUpService:  signUpService,
		signInService:  signInService,
		confirmService: confirmService,
		prolongService: prolongService,
		authService:    authService,
	}
}

func (h *AuthHandler) AuthConfirmUserPost(
	ctx context.Context,
	args openapi.ConfirmArgs,
) (openapi.ImplResponse, error) {
	_, err := h.confirmService.Process(ctx, &auth.ConfirmArgs{
		SessionToken: args.Session,
		Code:         args.Code,
	})
	if err != nil {
		return h.mapError(err)
	}

	return openapi.Response(http.StatusOK, nil), nil
}

func (h *AuthHandler) AuthSignInPost(
	ctx context.Context,
	args openapi.SignInArgs,
) (openapi.ImplResponse, error) {
	result, err := h.signInService.Process(ctx, &auth.SignInArgs{
		Email:    args.Email,
		Password: args.Password,
	})
	if err != nil {
		return h.mapError(err)
	}

	return openapi.Response(http.StatusOK, &openapi.SignInResult{
		Session: result.SessionToken,
	}), nil
}

func (h *AuthHandler) AuthSignUpPost(
	ctx context.Context,
	args openapi.SignUpArgs,
) (openapi.ImplResponse, error) {
	result, err := h.signUpService.Process(ctx, &auth.SignUpArgs{
		Email:    args.Email,
		Password: args.Password,
	})
	if err != nil {
		return h.mapError(err)
	}

	return openapi.Response(http.StatusCreated, &openapi.SignUpResult{
		Session: result.SessionToken,
	}), nil
}

func (h *AuthHandler) AuthProlongSessionPost(
	ctx context.Context,
	args openapi.ProlongSessionArgs,
) (openapi.ImplResponse, error) {
	sess, err := h.authService.Authorize(ctx)
	if err != nil {
		return dummyResponse, err
	}

	result, err := h.prolongService.Process(ctx, &auth.ProlongSessionArgs{
		Session:      sess,
		ProlongToken: args.ProlongToken,
	})
	if err != nil {
		return h.mapError(err)
	}

	return openapi.Response(http.StatusCreated, &openapi.ProlongSessionResult{
		Session:      result.SessionToken,
		ProlongToken: result.ProlongToken,
	}), nil
}

func (h *AuthHandler) mapError(e error) (openapi.ImplResponse, error) {
	if _, ok := e.(*auth.ErrUserDuplicate); ok {
		return openapi.ImplResponse{
			Code: http.StatusConflict,
			Body: openapi.UserAlreadyExists{},
		}, nil
	}

	if _, ok := e.(*auth.ErrUserDoesNotExist); ok {
		return openapi.ImplResponse{
			Code: http.StatusNotFound,
			Body: openapi.UserDoesNotExist{},
		}, nil
	}

	if _, ok := e.(*auth.ErrUserNotConfirmed); ok {
		return openapi.ImplResponse{
			Code: http.StatusBadRequest,
			Body: openapi.UserIsNotConfirmed{},
		}, nil
	}

	if _, ok := e.(*auth.ErrWrongConfirmationCode); ok {
		return openapi.ImplResponse{
			Code: http.StatusBadRequest,
			Body: openapi.UserConfirmationCodeIsInvalid{},
		}, nil
	}

	if _, ok := e.(*auth.ErrAlreadyConfirmed); ok {
		return openapi.ImplResponse{
			Code: http.StatusBadRequest,
			Body: openapi.UserIsAlreadyConfirmed{},
		}, nil
	}

	if _, ok := e.(*auth.ErrConfirmationExpired); ok {
		return openapi.ImplResponse{
			Code: http.StatusBadRequest,
			Body: openapi.UserConfirmationIsExpired{},
		}, nil
	}

	if _, ok := e.(*auth.ErrWrongProlongationToken); ok {
		return openapi.ImplResponse{
			Code: http.StatusBadRequest,
			Body: openapi.ProlongCodeInvalid{},
		}, nil
	}

	return dummyResponse, e
}
