package middlewares

import (
	"github.com/sirupsen/logrus"
	"github.com/valyala/fasthttp"
)

type LoggerMiddleware struct {
	logger *logrus.Logger
}

func NewLoggerMiddleware(logger *logrus.Logger) *LoggerMiddleware {
	return &LoggerMiddleware{
		logger: logger,
	}
}

func (l *LoggerMiddleware) Apply(next fasthttp.RequestHandler) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		l.logger.
			WithField("method", string(ctx.Method())).
			WithField("uri", ctx.Request.URI().String()).
			WithField("contentType", ctx.Request.Header.Peek(fasthttp.HeaderContentType)).
			WithField("query", ctx.Request.URI().QueryString()).
			WithField("body", ctx.Request.PostArgs().String()).
			Info("api request")

		next(ctx)

		l.logger.
			WithField("status", ctx.Response.StatusCode()).
			Info("api response", string(ctx.Response.Body()))
	}
}
