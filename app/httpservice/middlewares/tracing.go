package middlewares

import (
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/valyala/fasthttp"
	"gitlab.com/supply-demand/auth/dependency/tracing"
)

type TracingMiddleware struct {
	tracer opentracing.Tracer
}

func NewTracingMiddleware(tracer opentracing.Tracer) *TracingMiddleware {
	return &TracingMiddleware{
		tracer: tracer,
	}
}

func (l *TracingMiddleware) Apply(next fasthttp.RequestHandler) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		span := l.tracer.StartSpan(ctx.Request.String())
		defer span.Finish()

		ext.HTTPMethod.Set(span, string(ctx.Method()))
		ext.HTTPUrl.Set(span, ctx.URI().String())

		tracing.PutSpanToContext(ctx, span)

		next(ctx)

		span.SetTag(string(ext.HTTPStatusCode), ctx.Response.StatusCode())
	}
}
