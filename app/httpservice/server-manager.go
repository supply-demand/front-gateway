package httpservice

import (
	"fmt"

	"github.com/valyala/fasthttp"
)

type ServerManager struct {
	s       *fasthttp.Server
	cfg     *Config
	errChan chan error
}

func NewServerManager(s *fasthttp.Server, cfg *Config, errChan chan error) *ServerManager {
	return &ServerManager{
		s:       s,
		cfg:     cfg,
		errChan: errChan,
	}
}

func (m *ServerManager) Start() error {
	addr := fmt.Sprintf("%s:%d", m.cfg.ApiAddr, m.cfg.ApiPort)

	go func() {
		if err := m.s.ListenAndServe(addr); err != nil {
			m.errChan <- err
		}
	}()

	return nil
}

func (m *ServerManager) Stop() error {
	return m.s.Shutdown()
}
