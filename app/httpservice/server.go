package httpservice

import (
	"github.com/valyala/fasthttp"
)

func NewFastHttpServer(handler fasthttp.RequestHandler) (*fasthttp.Server, error) {
	s := &fasthttp.Server{
		Handler: handler,
	}

	return s, nil
}
