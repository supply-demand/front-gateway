package services

import (
	"context"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/supply-demand/auth/dependency/session"
)

type ErrUnauthorized struct {
}

func (e *ErrUnauthorized) Error() string {
	return "User is unauthorized"
}

const sessionLiveDuration = time.Minute * 30

const authorizationTokenCtxKey = "auth_token"

type AuthService struct {
	sessionStorage session.Storage
	logger         *logrus.Logger
}

func NewAuthService(sessionStorage session.Storage, logger *logrus.Logger) *AuthService {
	return &AuthService{
		sessionStorage: sessionStorage,
		logger:         logger,
	}
}

func (s *AuthService) Authorize(ctx context.Context) (*session.Session, error) {
	token := ctx.Value(authorizationTokenCtxKey)

	tokenStr, ok := token.(string)
	if !ok || tokenStr == "" {
		s.logger.Info("Authorization header is missing")
		return nil, &ErrUnauthorized{}
	}

	sess, err := s.sessionStorage.Get(ctx, tokenStr)
	if err != nil {
		s.logger.Info("Authorization token is invalid")
		return nil, &ErrUnauthorized{}
	}

	if sess.CreatedAt.Add(sessionLiveDuration).Before(time.Now()) {
		s.logger.Info("Authorization token is expired")
		return nil, &ErrUnauthorized{}
	}

	return sess, nil
}
