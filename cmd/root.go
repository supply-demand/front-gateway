package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/supply-demand/auth/app/httpservice"
)

func newServerStartCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "start",
		Short: "Start http server",
		RunE: func(cmd *cobra.Command, args []string) error {
			return httpservice.Run(cmd.Context())
		},
	}
}

func newRootCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "gateway",
		Short: "Front gateway",
		RunE: func(cmd *cobra.Command, args []string) error {
			return cmd.Usage()
		},
	}
}

func Run() {
	rootCmd := newRootCmd()
	rootCmd.AddCommand(newServerStartCmd())

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
	}
}
