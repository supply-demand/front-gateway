package redis

import (
	"github.com/Netflix/go-env"
)

type Config struct {
	Addr     string `env:"REDIS_ADDR"`
	DB       int    `env:"REDIS_DB"`
	Username string `env:"REDIS_USERNAME"`
	Password string `env:"REDIS_PASSWORD"`
}

func NewConfig() (*Config, error) {
	cfg := Config{}

	_, err := env.UnmarshalFromEnviron(&cfg)

	if err != nil {
		return nil, err
	}

	return &cfg, err
}
