package redis

import (
	"github.com/go-redis/redis/v8"
	"go.uber.org/fx"
)

func newRedisClient(c *Config) *redis.Client {
	return redis.NewClient(&redis.Options{
		Network:  c.Addr,
		DB:       c.DB,
		Username: c.Username,
		Password: c.Password,
	})
}

var Module = fx.Provide(
	NewConfig,
	newRedisClient,
)
