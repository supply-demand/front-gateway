package session

import "context"

type CustomSession interface {
	SetUserValue(key string, value interface{})
	Value(key interface{}) interface{}
}

const ContextKey = "auth-session"

func GetSessionFromContext(ctx context.Context) *Session {
	return ctx.Value(ContextKey).(*Session)
}
