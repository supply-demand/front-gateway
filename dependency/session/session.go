package session

import "time"

type Extras map[string]interface{}

type Session struct {
	Token        string
	ProlongToken string
	UserID       string
	Extras       *Extras
	CreatedAt    time.Time
}
