package session

import (
	"context"
	"encoding/json"
	"errors"
	"time"

	"github.com/go-redis/redis/v8"
	"gitlab.com/supply-demand/auth/shared/utils"
)

type ErrSessionDoesNotExist struct {
}

func (e *ErrSessionDoesNotExist) Error() string {
	return "SessionToken does not exist"
}

const Expiration = time.Minute * 60 * 24

const tokenLen = 10

type Storage interface {
	Create(ctx context.Context, session *Session) (string, error)
	Get(ctx context.Context, token string) (*Session, error)
	Del(ctx context.Context, token string) error
}

type RedisStorage struct {
	redisClient *redis.Client
}

func NewStorage(client *redis.Client) Storage {
	return &RedisStorage{redisClient: client}
}

func (s *RedisStorage) Create(ctx context.Context, session *Session) (string, error) {
	token := utils.GenerateRandomString(tokenLen)

	if session.ProlongToken == "" {
		session.ProlongToken = utils.GenerateRandomString(tokenLen)
	}

	err := s.Put(ctx, token, session)

	return token, err
}

func (s *RedisStorage) Put(ctx context.Context, token string, session *Session) error {
	sessionMarshalled, err := json.Marshal(session)
	if err != nil {
		return err
	}

	res := s.redisClient.Set(ctx, token, sessionMarshalled, Expiration)

	return res.Err()
}

func (s *RedisStorage) Get(ctx context.Context, token string) (*Session, error) {
	res := s.redisClient.Get(ctx, token)
	if err := res.Err(); err != nil {
		return nil, err
	}

	if res.Val() == "" {
		return nil, errors.New("session does not exist")
	}

	session := Session{}

	err := json.Unmarshal([]byte(res.Val()), &session)
	if err != nil {
		return nil, &ErrSessionDoesNotExist{}
	}

	return &session, nil
}

func (s *RedisStorage) Del(ctx context.Context, token string) error {
	res := s.redisClient.Del(ctx, token)
	return res.Err()
}
