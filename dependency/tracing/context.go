package tracing

import "github.com/opentracing/opentracing-go"

type CustomSession interface {
	SetUserValue(key string, value interface{})
	Value(key interface{}) interface{}
}

const contextKey = "opentracing.span"

func GetSpanFromContext(ctx CustomSession) opentracing.Span {
	return ctx.Value(contextKey).(opentracing.Span)
}

func PutSpanToContext(ctx CustomSession, span opentracing.Span) {
	ctx.SetUserValue(contextKey, span)
}
