package auth

import (
	"errors"
	"time"

	"gitlab.com/supply-demand/auth/dependency/session"
	"gitlab.com/supply-demand/auth/shared/utils"
)

const tokenLen = 10

type ActiveSession struct {
	UserID       string
	Email        string
	Phone        string
	Name         string
	ProlongToken string
	CreatedAt    time.Time
}

func NewActiveSessionFromUser(user *User) *ActiveSession {
	return &ActiveSession{
		UserID:       user.ID,
		Email:        user.Email,
		Name:         user.Name,
		ProlongToken: utils.GenerateRandomString(tokenLen),
		CreatedAt:    user.CreatedAt,
	}
}

func NewActiveSessionFromRaw(raw *session.Session) (*ActiveSession, error) {
	if raw.Extras == nil {
		return nil, errors.New("could not populate active session")
	}

	instance := &ActiveSession{}
	instance.UserID = raw.UserID
	instance.ProlongToken = raw.ProlongToken
	instance.CreatedAt = raw.CreatedAt

	instance.Email = (*raw.Extras)["email"].(string)
	instance.Phone = (*raw.Extras)["phone"].(string)
	instance.Name = (*raw.Extras)["name"].(string)

	return instance, nil
}

func (s *ActiveSession) ToRaw() *session.Session {
	return &session.Session{
		UserID: s.UserID,
		Extras: &session.Extras{
			"email":        s.Email,
			"phone":        s.Phone,
			"name":         s.Name,
			"prolongToken": s.ProlongToken,
			"createdAt":    s.CreatedAt.Unix(),
		},
	}
}
