package auth

import (
	"context"

	"gitlab.com/supply-demand/auth/dependency/session"

	userProto "gitlab.com/supply-demand/proto_gen/go/user"
)

//

type ErrWrongConfirmationCode struct {
}

func (e *ErrWrongConfirmationCode) Error() string {
	return "Confirmation code is invalid"
}

//

type ErrConfirmationExpired struct {
}

func (e *ErrConfirmationExpired) Error() string {
	return "Confirmation is expired"
}

//

type ErrAlreadyConfirmed struct {
}

func (e *ErrAlreadyConfirmed) Error() string {
	return "User is already confirmed"
}

//

type ConfirmArgs struct {
	SessionToken string
	Code         int32
}

type ConfirmResult struct {
	ETag string
}

type ConfirmService struct {
	client         userProto.UserServiceClient
	sessionService session.Storage
	userService    *UserService
}

func NewConfirmService(client userProto.UserServiceClient, sessionService session.Storage, userService *UserService) *ConfirmService {
	return &ConfirmService{
		client:         client,
		sessionService: sessionService,
		userService:    userService,
	}
}

func (s *ConfirmService) Process(ctx context.Context, args *ConfirmArgs) (*ConfirmResult, error) {
	rawSession, err := s.sessionService.Get(ctx, args.SessionToken)
	if err != nil {
		return nil, err
	}

	confirmationSession, err := NewConfirmationSessionFromRaw(rawSession)
	if err != nil {
		return nil, err
	}

	if confirmationSession.Code != args.Code {
		return nil, &ErrWrongConfirmationCode{}
	}

	found, err := s.client.FindByID(ctx, &userProto.FindByIDArgs{
		Id: confirmationSession.UserID,
	})
	if err != nil {
		return nil, MapProtoError(err)
	}

	user := NewUserFromProto(found.GetUser())

	if s.userService.IsConfirmed(user) {
		return nil, &ErrAlreadyConfirmed{}
	}

	if s.userService.IsConfirmationExpired(user) {
		return nil, &ErrConfirmationExpired{}
	}

	statusChanged, err := s.client.ChangeStatus(ctx, &userProto.ChangeStatusArgs{
		Id:     user.ID,
		Status: MapUserStatus(StatusConfirmed),
		Etag:   found.GetUser().GetEtag(),
	})
	if err != nil {
		return nil, MapProtoError(err)
	}

	return &ConfirmResult{
		ETag: statusChanged.Etag,
	}, nil
}
