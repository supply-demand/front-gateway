package auth

import (
	"fmt"
	"github.com/pkg/errors"
	"strconv"
	"time"

	"gitlab.com/supply-demand/auth/dependency/session"
	"gitlab.com/supply-demand/auth/shared/utils"
)

type ConfirmationSession struct {
	UserID    string
	Code      int32
	CreatedAt time.Time
}

func NewConfirmationSessionFromUserID(userID string) *ConfirmationSession {
	return &ConfirmationSession{
		UserID: userID,
		Code:   utils.GenerateIntBetween(10000, 99999),
	}
}

func NewConfirmationSessionFromRaw(raw *session.Session) (*ConfirmationSession, error) {
	if raw.Extras == nil {
		return nil, errors.New("could not populate session")
	}

	instance := &ConfirmationSession{}
	instance.UserID = raw.UserID
	instance.CreatedAt = raw.CreatedAt
	code := (*raw.Extras)["confirmationCode"]

	codeInt, err := strconv.ParseInt(fmt.Sprintf("%v", code), 10, 64)
	if err != nil {
		return nil, errors.New("could not convert confirmation code to integer")
	}

	instance.Code = int32(codeInt)

	return instance, nil
}

func (s *ConfirmationSession) ToRaw() *session.Session {
	return &session.Session{
		UserID: s.UserID,
		Extras: &session.Extras{
			"confirmationCode": s.Code,
		},
	}
}

func (s *ConfirmationSession) GetCode() int32 {
	return s.Code
}
