package auth

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

//

type ErrUserDuplicate struct {
}

func NewErrUserDuplicate() *ErrUserDuplicate {
	return &ErrUserDuplicate{}
}

func (e *ErrUserDuplicate) Error() string {
	return "User already exists"
}

//

type ErrUserDoesNotExist struct {
}

func NewErrUserDoesNotExist() *ErrUserDoesNotExist {
	return &ErrUserDoesNotExist{}
}

func (e *ErrUserDoesNotExist) Error() string {
	return "User does not exist"
}

//

func MapProtoError(err error) error {
	if isUserDuplicate(err) {
		return NewErrUserDuplicate()
	} else if isUserNotFound(err) {
		return NewErrUserDoesNotExist()
	}

	return err
}

func isUserDuplicate(err error) bool {
	grpcErr, ok := status.FromError(err)
	if !ok {
		return false
	}

	return grpcErr.Code() == codes.AlreadyExists
}

func isUserNotFound(err error) bool {
	grpcErr, ok := status.FromError(err)
	if !ok {
		return false
	}

	return grpcErr.Code() == codes.NotFound
}
