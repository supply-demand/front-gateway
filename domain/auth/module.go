package auth

import "go.uber.org/fx"

var Module = fx.Provide(
	NewPasswordService,
	NewSignUpService,
	NewSignInService,
	NewConfirmService,
	NewProlongSessionService,
	NewUserService,
)
