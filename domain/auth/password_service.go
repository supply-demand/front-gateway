package auth

import (
	"golang.org/x/crypto/bcrypt"
)

type PasswordService struct {
}

func NewPasswordService() *PasswordService {
	return &PasswordService{}
}

func (s *PasswordService) Hash(v string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(v), bcrypt.MinCost)
	return string(bytes), err
}

func (s *PasswordService) Compare(hash string, v string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(v))
	return err == nil
}
