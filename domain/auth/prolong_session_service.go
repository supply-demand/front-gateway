package auth

import (
	"context"

	"gitlab.com/supply-demand/auth/dependency/session"

	userProto "gitlab.com/supply-demand/proto_gen/go/user"
)

type ErrWrongProlongationToken struct {
}

func (e *ErrWrongProlongationToken) Error() string {
	return "Prolongation session token is invalid"
}

//

type ProlongSessionArgs struct {
	Session      *session.Session
	ProlongToken string
}

type ProlongSessionResult struct {
	SessionToken string
	ProlongToken string
}

type ProlongSessionService struct {
	client         userProto.UserServiceClient
	sessionService session.Storage
	userService    *UserService
}

func NewProlongSessionService(sessionService session.Storage, userService *UserService, client userProto.UserServiceClient) *ProlongSessionService {
	return &ProlongSessionService{
		client:         client,
		sessionService: sessionService,
		userService:    userService,
	}
}

func (s *ProlongSessionService) Process(ctx context.Context, args *ProlongSessionArgs) (*ProlongSessionResult, error) {
	if args.Session.ProlongToken != args.ProlongToken {
		return nil, &ErrWrongProlongationToken{}
	}

	found, err := s.client.FindByID(ctx, &userProto.FindByIDArgs{
		Id: args.Session.UserID,
	})
	if err != nil {
		return nil, MapProtoError(err)
	}

	user := NewUserFromProto(found.GetUser())
	if !s.userService.IsConfirmed(user) {
		return nil, &ErrUserNotConfirmed{}
	}

	newSess := NewActiveSessionFromUser(user)

	newToken, err := s.sessionService.Create(ctx, newSess.ToRaw())
	if err != nil {
		return nil, err
	}

	return &ProlongSessionResult{
		SessionToken: newToken,
		ProlongToken: newSess.ProlongToken,
	}, nil
}
