package auth

import (
	"context"

	"gitlab.com/supply-demand/auth/dependency/session"

	userProto "gitlab.com/supply-demand/proto_gen/go/user"
)

type ErrUserNotConfirmed struct {
}

func (e *ErrUserNotConfirmed) Error() string {
	return "User is not confirmed"
}

//

type SignInArgs struct {
	Email    string
	Password string
}

type SignInResult struct {
	User         *User
	SessionToken string
}

type SignInService struct {
	client          userProto.UserServiceClient
	sessionService  session.Storage
	passwordService *PasswordService
	userService     *UserService
}

func NewSignInService(
	client userProto.UserServiceClient,
	sessionStorage session.Storage,
	passwordService *PasswordService,
	userService *UserService,
) *SignInService {
	return &SignInService{
		client:          client,
		sessionService:  sessionStorage,
		passwordService: passwordService,
		userService:     userService,
	}
}

func (s *SignInService) Process(
	ctx context.Context,
	args *SignInArgs,
) (*SignInResult, error) {
	passwordHash, err := s.passwordService.Hash(args.Password)
	if err != nil {
		return nil, err
	}

	found, err := s.client.FindByEmailAndPassword(ctx, &userProto.FindByEmailAndPasswordArgs{
		Email:        args.Email,
		PasswordHash: passwordHash,
	})
	if err != nil {
		return nil, MapProtoError(err)
	}

	user := NewUserFromProto(found.GetUser())
	if !s.userService.IsConfirmed(user) {
		return nil, &ErrUserNotConfirmed{}
	}

	sess := NewActiveSessionFromUser(user)
	sessionToken, err := s.sessionService.Create(ctx, sess.ToRaw())
	if err != nil {
		return nil, err
	}

	return &SignInResult{
		User:         user,
		SessionToken: sessionToken,
	}, nil
}
