package auth

import (
	"context"

	"gitlab.com/supply-demand/auth/dependency/session"

	userProto "gitlab.com/supply-demand/proto_gen/go/user"
)

type SignUpArgs struct {
	Email    string
	Name     string
	Phone    string
	Password string
}

type SignUpResult struct {
	SessionToken     string
	ConfirmationCode int32
}

type SignUpService struct {
	client          userProto.UserServiceClient
	sessionService  session.Storage
	passwordService *PasswordService
	userService     *UserService
}

func NewSignUpService(
	client userProto.UserServiceClient,
	sessionStorage session.Storage,
	passwordService *PasswordService,
	userService *UserService,
) *SignUpService {
	return &SignUpService{
		client:          client,
		sessionService:  sessionStorage,
		passwordService: passwordService,
		userService:     userService,
	}
}

// Process TODO: cache password hash
func (s *SignUpService) Process(ctx context.Context, args *SignUpArgs) (*SignUpResult, error) {
	result, err := s.perform(ctx, args)
	if err == nil {
		return result, nil
	}

	errDuplicated, _ := err.(*ErrUserDuplicate)

	if errDuplicated != nil {
		isDropped, err := s.dropDuplicateIfConfirmationExpired(ctx, args.Email, args.Password)
		if err != nil {
			return nil, err
		}

		if isDropped {
			return s.perform(ctx, args)
		}
	}

	return nil, err
}

func (s *SignUpService) perform(ctx context.Context, args *SignUpArgs) (*SignUpResult, error) {
	pass, err := s.passwordService.Hash(args.Password)
	if err != nil {
		return nil, err
	}

	result, err := s.client.Create(ctx, &userProto.CreateArgs{
		Name:         args.Name,
		Email:        args.Email,
		PasswordHash: pass,
		Phone:        args.Phone,
	})
	if err != nil {
		return nil, MapProtoError(err)
	}

	sess := NewConfirmationSessionFromUserID(result.UserID)

	sessionToken, err := s.sessionService.Create(ctx, sess.ToRaw())
	if err != nil {
		return nil, err
	}

	return &SignUpResult{
		SessionToken:     sessionToken,
		ConfirmationCode: sess.GetCode(),
	}, nil
}

// TODO: retry in case of etag error
// dropDuplicateIfConfirmationExpired returns if user is dropped
func (s *SignUpService) dropDuplicateIfConfirmationExpired(
	ctx context.Context,
	email string,
	pass string,
) (bool, error) {
	found, err := s.client.FindByEmailAndPassword(ctx, &userProto.FindByEmailAndPasswordArgs{
		Email:        email,
		PasswordHash: pass,
	})
	if err != nil {
		return false, err
	}

	user := NewUserFromProto(found.GetUser())

	if !s.userService.HasToBeDropped(user) {
		return false, nil
	}

	_, err = s.client.Remove(ctx, &userProto.RemoveArgs{
		Id:   found.GetUser().GetId(),
		Etag: found.GetUser().GetEtag(),
	})
	if err != nil {
		return false, err
	}

	return true, nil
}
