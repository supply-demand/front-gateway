package auth

import (
	"time"

	userProto "gitlab.com/supply-demand/proto_gen/go/user"
)

type UserStatus int32

const (
	StatusNew       UserStatus = 0
	StatusConfirmed UserStatus = 1
	StatusRemoved   UserStatus = 2
)

type User struct {
	ID           string
	Email        string
	Name         string
	Phone        string
	PasswordHash string
	Status       UserStatus
	CreatedAt    time.Time
}

func NewUserFromProto(u *userProto.User) *User {
	return &User{
		ID:           u.GetId(),
		Email:        u.GetEmail(),
		Name:         u.GetName(),
		PasswordHash: u.GetPasswordHash(),
		Phone:        u.GetPhone(),
		Status:       UserStatus(u.GetStatus()),
		CreatedAt:    u.GetCreatedAt().AsTime(),
	}
}

func MapUserStatus(s UserStatus) userProto.Status {
	return userProto.Status(s)
}
