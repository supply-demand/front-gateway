package auth

import (
	"time"

	"gitlab.com/supply-demand/auth/dependency/clock"
)

const confirmationDuration = time.Minute * 15

type UserService struct {
	clock clock.Clock
}

func NewUserService(clock clock.Clock) *UserService {
	return &UserService{
		clock: clock,
	}
}

func (s *UserService) IsConfirmationExpired(entity *User) bool {
	expiredAfter := entity.CreatedAt.Add(confirmationDuration)
	return expiredAfter.Before(s.clock.Now())
}

func (s *UserService) HasToBeDropped(entity *User) bool {
	if entity.Status != StatusNew {
		return false
	}

	return s.IsConfirmationExpired(entity)
}

func (s *UserService) IsConfirmed(entity *User) bool {
	return entity.Status != StatusNew
}
