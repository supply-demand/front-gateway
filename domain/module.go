package domain

import (
	"gitlab.com/supply-demand/auth/domain/auth"
	"go.uber.org/fx"
)

var Module = fx.Options(
	auth.Module,
)
