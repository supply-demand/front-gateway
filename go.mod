module gitlab.com/supply-demand/auth

go 1.16

require (
	github.com/HdrHistogram/hdrhistogram-go v1.1.2 // indirect
	github.com/Netflix/go-env v0.0.0-20210215222557-e437a7e7f9fb
	github.com/andybalholm/brotli v1.0.3 // indirect
	github.com/cucumber/godog v0.12.0
	github.com/fasthttp/router v1.4.3
	github.com/go-redis/redis/v8 v8.11.3
	github.com/golang/mock v1.6.0
	github.com/gorilla/mux v1.8.0
	github.com/klauspost/compress v1.13.6 // indirect
	github.com/nsf/jsondiff v0.0.0-20210926074059-1e845ec5d249 // indirect
	github.com/opentracing/opentracing-go v1.2.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.2.1
	github.com/stretchr/testify v1.7.0
	github.com/uber/jaeger-client-go v2.29.1+incompatible
	github.com/uber/jaeger-lib v2.4.1+incompatible // indirect
	github.com/valyala/fasthttp v1.30.0
	gitlab.com/supply-demand/proto_gen/go/user v0.0.7
	go.uber.org/fx v1.14.2
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
	golang.org/x/net v0.0.0-20211008194852-3b03d305991f // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/grpc v1.40.0
)
