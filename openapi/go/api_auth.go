/*
 * Front gateway
 *
 * Front gateway
 *
 * API version: 0.0.1
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package openapi

import (
	"encoding/json"
	"net/http"
	"strings"
)

// AuthApiController binds http requests to an api service and writes the service results to the http response
type AuthApiController struct {
	service      AuthApiServicer
	errorHandler ErrorHandler
}

// AuthApiOption for how the controller is set up.
type AuthApiOption func(*AuthApiController)

// WithAuthApiErrorHandler inject ErrorHandler into controller
func WithAuthApiErrorHandler(h ErrorHandler) AuthApiOption {
	return func(c *AuthApiController) {
		c.errorHandler = h
	}
}

// NewAuthApiController creates a default api controller
func NewAuthApiController(s AuthApiServicer, opts ...AuthApiOption) Router {
	controller := &AuthApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}

	for _, opt := range opts {
		opt(controller)
	}

	return controller
}

// Routes returns all of the api route for the AuthApiController
func (c *AuthApiController) Routes() Routes {
	return Routes{
		{
			"AuthConfirmUserPost",
			strings.ToUpper("Post"),
			"/auth/confirm-user",
			c.AuthConfirmUserPost,
		},
		{
			"AuthProlongSessionPost",
			strings.ToUpper("Post"),
			"/auth/prolong-session",
			c.AuthProlongSessionPost,
		},
		{
			"AuthSignInPost",
			strings.ToUpper("Post"),
			"/auth/sign-in",
			c.AuthSignInPost,
		},
		{
			"AuthSignUpPost",
			strings.ToUpper("Post"),
			"/auth/sign-up",
			c.AuthSignUpPost,
		},
	}
}

// AuthConfirmUserPost -
func (c *AuthApiController) AuthConfirmUserPost(w http.ResponseWriter, r *http.Request) {
	confirmArgs := ConfirmArgs{}
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields()
	if err := d.Decode(&confirmArgs); err != nil {
		c.errorHandler(w, r, &ParsingError{Err: err}, nil)
		return
	}
	if err := AssertConfirmArgsRequired(confirmArgs); err != nil {
		c.errorHandler(w, r, err, nil)
		return
	}
	result, err := c.service.AuthConfirmUserPost(r.Context(), confirmArgs)
	// If an error occurred, encode the error with the status code
	if err != nil {
		c.errorHandler(w, r, err, &result)
		return
	}
	// If no error, encode the body and the result code
	EncodeJSONResponse(result.Body, &result.Code, w)

}

// AuthProlongSessionPost -
func (c *AuthApiController) AuthProlongSessionPost(w http.ResponseWriter, r *http.Request) {
	prolongSessionArgs := ProlongSessionArgs{}
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields()
	if err := d.Decode(&prolongSessionArgs); err != nil {
		c.errorHandler(w, r, &ParsingError{Err: err}, nil)
		return
	}
	if err := AssertProlongSessionArgsRequired(prolongSessionArgs); err != nil {
		c.errorHandler(w, r, err, nil)
		return
	}
	result, err := c.service.AuthProlongSessionPost(r.Context(), prolongSessionArgs)
	// If an error occurred, encode the error with the status code
	if err != nil {
		c.errorHandler(w, r, err, &result)
		return
	}
	// If no error, encode the body and the result code
	EncodeJSONResponse(result.Body, &result.Code, w)

}

// AuthSignInPost -
func (c *AuthApiController) AuthSignInPost(w http.ResponseWriter, r *http.Request) {
	signInArgs := SignInArgs{}
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields()
	if err := d.Decode(&signInArgs); err != nil {
		c.errorHandler(w, r, &ParsingError{Err: err}, nil)
		return
	}
	if err := AssertSignInArgsRequired(signInArgs); err != nil {
		c.errorHandler(w, r, err, nil)
		return
	}
	result, err := c.service.AuthSignInPost(r.Context(), signInArgs)
	// If an error occurred, encode the error with the status code
	if err != nil {
		c.errorHandler(w, r, err, &result)
		return
	}
	// If no error, encode the body and the result code
	EncodeJSONResponse(result.Body, &result.Code, w)

}

// AuthSignUpPost -
func (c *AuthApiController) AuthSignUpPost(w http.ResponseWriter, r *http.Request) {
	signUpArgs := SignUpArgs{}
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields()
	if err := d.Decode(&signUpArgs); err != nil {
		c.errorHandler(w, r, &ParsingError{Err: err}, nil)
		return
	}
	if err := AssertSignUpArgsRequired(signUpArgs); err != nil {
		c.errorHandler(w, r, err, nil)
		return
	}
	result, err := c.service.AuthSignUpPost(r.Context(), signUpArgs)
	// If an error occurred, encode the error with the status code
	if err != nil {
		c.errorHandler(w, r, err, &result)
		return
	}
	// If no error, encode the body and the result code
	EncodeJSONResponse(result.Body, &result.Code, w)

}
