/*
 * Front gateway
 *
 * Front gateway
 *
 * API version: 0.0.1
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package openapi

type UserDoesNotExist struct {
	UserDoesNotExist map[string]interface{} `json:"userDoesNotExist,omitempty"`
}

// AssertUserDoesNotExistRequired checks if the required fields are not zero-ed
func AssertUserDoesNotExistRequired(obj UserDoesNotExist) error {
	return nil
}

// AssertRecurseUserDoesNotExistRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of UserDoesNotExist (e.g. [][]UserDoesNotExist), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseUserDoesNotExistRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aUserDoesNotExist, ok := obj.(UserDoesNotExist)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertUserDoesNotExistRequired(aUserDoesNotExist)
	})
}
