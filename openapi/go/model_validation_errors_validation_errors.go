/*
 * Front gateway
 *
 * Front gateway
 *
 * API version: 0.0.1
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package openapi

type ValidationErrorsValidationErrors struct {
	Key string `json:"key,omitempty"`

	Rule string `json:"rule,omitempty"`

	Message string `json:"message,omitempty"`
}

// AssertValidationErrorsValidationErrorsRequired checks if the required fields are not zero-ed
func AssertValidationErrorsValidationErrorsRequired(obj ValidationErrorsValidationErrors) error {
	return nil
}

// AssertRecurseValidationErrorsValidationErrorsRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of ValidationErrorsValidationErrors (e.g. [][]ValidationErrorsValidationErrors), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseValidationErrorsValidationErrorsRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aValidationErrorsValidationErrors, ok := obj.(ValidationErrorsValidationErrors)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertValidationErrorsValidationErrorsRequired(aValidationErrorsValidationErrors)
	})
}
