package utils

import "math/rand"

func GenerateIntBetween(min int, max int) int32 {
	return int32(rand.Intn(max-min) + min)
}
