package contexts

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	"github.com/cucumber/godog"
	"github.com/stretchr/testify/assert"
)

type TCPAddress string

type APIContext struct {
	baseURL     TCPAddress
	test        *testing.T
	apiResponse *http.Response
}

func NewAPIContext(test *testing.T, baseURL TCPAddress) *APIContext {
	return &APIContext{
		baseURL: baseURL,
		test:    test,
	}
}

func (h *APIContext) Register(sc *godog.ScenarioContext) {
	sc.Step(`^I send API request to GET "([^"]*)"`, h.sendGetApiRequest)
	sc.Step(`^I send API request to POST "([^"]*)" with$`, h.sendPostApiRequest)
	sc.Step(`^I see API response status "([^"]*)"$`, h.iSeeAPIResponseStatus)
	sc.Step(`^I see API response body$`, h.iSeeAPIResponseBody)
}

func (h *APIContext) sendGetApiRequest(urlpath string) {
	// TODO:
}

func (h *APIContext) iSeeAPIResponseBody(data *godog.DocString) error {
	body, err := ioutil.ReadAll(h.apiResponse.Body)
	if err != nil {
		return err
	}

	assert.JSONEqf(h.test, data.Content, string(body), "Response body must be equal")

	return nil
}

func (h *APIContext) iSeeAPIResponseStatus(status int) error {
	assert.Equal(h.test, status, h.apiResponse.StatusCode)

	return nil
}

func (h *APIContext) sendPostApiRequest(urlPath string, data *godog.DocString) error {
	fullURL := fmt.Sprintf("http://%s%s", h.baseURL, urlPath)
	body := bytes.NewReader([]byte(data.Content))

	client := http.Client{
		Timeout: 10 * time.Second,
	}

	response, err := client.Post(fullURL, "application/json", body)
	h.apiResponse = response

	return err
}
