package contexts

import (
	"testing"
	"time"

	"github.com/cucumber/godog"
	"gitlab.com/supply-demand/auth/tests/mocks"
)

type ClockContext struct {
	test      *testing.T
	clockMock *mocks.MockClock
}

func NewClockContext(test *testing.T, clockMock *mocks.MockClock) *ClockContext {
	return &ClockContext{
		test:      test,
		clockMock: clockMock,
	}
}

func (c *ClockContext) Register(sc *godog.ScenarioContext) {
	sc.Step(`^I check current time I see "([^"]*)"`, c.currentTimeIs)
}

func (c *ClockContext) currentTimeIs(v string) error {
	t, err := time.Parse(time.RFC3339, v)
	if err != nil {
		return err
	}

	c.clockMock.EXPECT().Now().Return(t)

	return nil
}
