package contexts

import (
	"encoding/json"
	"github.com/cucumber/godog"
	"github.com/golang/mock/gomock"
	"gitlab.com/supply-demand/auth/dependency/session"
	"gitlab.com/supply-demand/auth/tests/mocks"
)

type SessionServiceContext struct {
	ctrl *gomock.Controller
	mock *mocks.MockStorage
}

func NewSessionServiceContext(ctrl *gomock.Controller, mock *mocks.MockStorage) *SessionServiceContext {
	return &SessionServiceContext{
		ctrl: ctrl,
		mock: mock,
	}
}

func (h *SessionServiceContext) Register(sc *godog.ScenarioContext) {
	sc.Step(`^I save new session I see "([^"]*)"$`, h.iSaveNewSessionISee)
	sc.Step(`^I retrieve session by "([^"]*)" token I see$`, h.iRetrieveSessionByTokenISee)
}

func (h *SessionServiceContext) GetMock() *mocks.MockStorage {
	return h.mock
}

func (h *SessionServiceContext) iSaveNewSessionISee(token string) error {
	h.mock.
		EXPECT().
		Create(gomock.Any(), gomock.Any()).
		Return(token, nil).
		Times(1)

	return nil
}

func (h *SessionServiceContext) iRetrieveSessionByTokenISee(token string, mock *godog.DocString) error {
	sess := &session.Session{}
	if err := json.Unmarshal([]byte(mock.Content), sess); err != nil {
		return err
	}

	h.mock.
		EXPECT().
		Get(gomock.Any(), token).
		Return(sess, nil).
		Times(1)

	return nil
}
