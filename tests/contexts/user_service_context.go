package contexts

import (
	"encoding/json"
	"strconv"

	"github.com/cucumber/godog"
	"github.com/golang/mock/gomock"
	"gitlab.com/supply-demand/auth/tests/mocks"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	userProto "gitlab.com/supply-demand/proto_gen/go/user"
)

type UserServiceContext struct {
	ctrl                  *gomock.Controller
	userServiceClientMock *mocks.MockUserServiceClient
}

func NewUserServiceContext(
	ctrl *gomock.Controller,
	userServiceClientMock *mocks.MockUserServiceClient,
) *UserServiceContext {
	return &UserServiceContext{
		ctrl:                  ctrl,
		userServiceClientMock: userServiceClientMock,
	}
}

func (h *UserServiceContext) GetMock() *mocks.MockUserServiceClient {
	return h.userServiceClientMock
}

func (h *UserServiceContext) Register(sc *godog.ScenarioContext) {
	sc.Step(`^I save new user I see error code "([^"]*)"$`, h.iSaveNewUserISeeErrorCode)
	sc.Step(`^I save new user I see response$`, h.iSaveNewUserISeeResponse)
	sc.Step(`^I look for user by email and password I see response$`, h.iLookForUserByEmailAndPasswordISeeResponse)
	sc.Step(`^I look for user by id "([^"]*)" I see response$`, h.iLookForUserByIDISeeResponse)
	sc.Step(`^I remove user I see response$`, h.iRemoveUserISeeResponse)
	sc.Step(`^I change user "([^"]*)" status to "([^"]*)" I see response$`, h.iChangeUserStatusISeeResponse)
}

func (h *UserServiceContext) iSaveNewUserISeeResponse(mock *godog.DocString) error {
	resp := &userProto.CreateResult{}
	if err := json.Unmarshal([]byte(mock.Content), resp); err != nil {
		return err
	}

	h.userServiceClientMock.
		EXPECT().
		Create(gomock.Any(), gomock.Any()).
		Return(resp, nil).
		Times(1)

	return nil
}

func (h *UserServiceContext) iLookForUserByEmailAndPasswordISeeResponse(mock *godog.DocString) error {
	resp := &userProto.FindByEmailAndPasswordResult{}
	if err := json.Unmarshal([]byte(mock.Content), resp); err != nil {
		return err
	}

	h.userServiceClientMock.
		EXPECT().
		FindByEmailAndPassword(gomock.Any(), gomock.Any()).
		Return(resp, nil).
		Times(1)

	return nil
}

func (h *UserServiceContext) iLookForUserByIDISeeResponse(id string, mock *godog.DocString) error {
	args := &userProto.FindByIDArgs{}
	args.Id = id

	result := &userProto.FindByIDResult{}
	if err := json.Unmarshal([]byte(mock.Content), result); err != nil {
		return err
	}

	h.userServiceClientMock.
		EXPECT().
		FindByID(gomock.Any(), args).
		Return(result, nil).
		Times(1)

	return nil
}

func (h *UserServiceContext) iRemoveUserISeeResponse(mock *godog.DocString) error {
	resp := &userProto.RemoveResult{}
	if err := json.Unmarshal([]byte(mock.Content), resp); err != nil {
		return err
	}

	h.userServiceClientMock.
		EXPECT().
		Remove(gomock.Any(), gomock.Any()).
		Return(resp, nil).
		Times(1)

	return nil
}

func (h *UserServiceContext) iSaveNewUserISeeErrorCode(code int) error {
	resp := &userProto.CreateResult{}
	grpcStatusErr := status.New(codes.Code(code), "Test grpc status message").Err()

	h.userServiceClientMock.
		EXPECT().
		Create(gomock.Any(), gomock.Any()).
		Return(resp, grpcStatusErr).
		Times(1)

	return nil
}

func (h *UserServiceContext) iChangeUserStatusISeeResponse(id string, status string, mock *godog.DocString) error {
	statusInt, err := strconv.Atoi(status)
	if err != nil {
		return err
	}

	args := &userProto.ChangeStatusArgs{
		Id:     id,
		Status: userProto.Status(statusInt),
		Etag:   "0000000000",
	}
	resp := &userProto.ChangeStatusResult{}
	if err := json.Unmarshal([]byte(mock.Content), resp); err != nil {
		return err
	}

	h.userServiceClientMock.
		EXPECT().
		ChangeStatus(gomock.Any(), args).
		Return(resp, nil).
		Times(1)

	return nil
}
