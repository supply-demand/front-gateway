Feature: User's confirmation

  Scenario: Confirm user successfully
    When I retrieve session by "qwerty" token I see
    """
    {
      "token": "qwerty",
      "prolongToken": "bbb",
      "userId": "6aaa21cf-6734-4b4e-a7a4-2740c01b6418",
      "createdAt": "2018-09-22T16:35:08+00:00",
      "extras": {
        "confirmationCode": 5544
      }
    }
    """

    When I look for user by id "6aaa21cf-6734-4b4e-a7a4-2740c01b6418" I see response
    """
    {
      "user": {
        "id": "6aaa21cf-6734-4b4e-a7a4-2740c01b6418",
        "email": "foobar@gmail.com",
        "name": "Foo",
        "phone": "+12345678",
        "passwordHash": "aBk2KlcA0sow",
        "status": 0,
        "createdAt": {
          "seconds": 1635690671,
          "nanos": 0
        },
        "etag": "0000000000"
      }
    }
    """

    When I check current time I see "2021-10-31T14:32:11+00:00"

    When I change user "6aaa21cf-6734-4b4e-a7a4-2740c01b6418" status to "1" I see response
    """
    {
      "etag": "000000"
    }
    """

    Then I send API request to POST "/auth/confirm-user" with
    """
    {
      "session": "qwerty",
      "code": 5544
    }
    """

    Then I see API response status "200"

  Scenario: Confirmation is expired
    When I retrieve session by "qwerty" token I see
    """
    {
      "token": "qwerty",
      "prolongToken": "bbb",
      "userId": "6aaa21cf-6734-4b4e-a7a4-2740c01b6418",
      "createdAt": "2018-09-22T16:35:08+00:00",
      "extras": {
        "confirmationCode": 5544
      }
    }
    """

    When I look for user by id "6aaa21cf-6734-4b4e-a7a4-2740c01b6418" I see response
    """
    {
      "user": {
        "id": "6aaa21cf-6734-4b4e-a7a4-2740c01b6418",
        "email": "foobar@gmail.com",
        "name": "Foo",
        "phone": "+12345678",
        "passwordHash": "aBk2KlcA0sow",
        "status": 0,
        "createdAt": {
          "seconds": 1635690671,
          "nanos": 0
        },
        "etag": "0000000000"
      }
    }
    """

    When I check current time I see "2021-10-31T14:50:11+00:00"

    Then I send API request to POST "/auth/confirm-user" with
    """
    {
      "session": "qwerty",
      "code": 5544
    }
    """

    Then I see API response status "400"

  Scenario: Confirmation code matches the code from auth session
    When I retrieve session by "qwerty" token I see
    """
    {
      "token": "qwerty",
      "prolongToken": "bbb",
      "userId": "6aaa21cf-6734-4b4e-a7a4-2740c01b6418",
      "createdAt": "2018-09-22T16:35:08+00:00",
      "extras": {
        "confirmationCode": 1212
      }
    }
    """

    Then I send API request to POST "/auth/confirm-user" with
    """
    {
      "session": "qwerty",
      "code": 5544
    }
    """

    Then I see API response status "400"

  Scenario: User is already confirmed
    When I retrieve session by "qwerty" token I see
    """
    {
      "token": "qwerty",
      "prolongToken": "bbb",
      "userId": "6aaa21cf-6734-4b4e-a7a4-2740c01b6418",
      "createdAt": "2018-09-22T16:35:08+00:00",
      "extras": {
        "confirmationCode": 5544
      }
    }
    """

    When I look for user by id "6aaa21cf-6734-4b4e-a7a4-2740c01b6418" I see response
    """
    {
      "user": {
        "id": "6aaa21cf-6734-4b4e-a7a4-2740c01b6418",
        "email": "foobar@gmail.com",
        "name": "Foo",
        "phone": "+12345678",
        "passwordHash": "aBk2KlcA0sow",
        "status": 1,
        "createdAt": {
          "seconds": 1635690671,
          "nanos": 0
        },
        "etag": "0000000000"
      }
    }
    """

    Then I send API request to POST "/auth/confirm-user" with
    """
    {
      "session": "qwerty",
      "code": 5544
    }
    """

    Then I see API response status "400"
