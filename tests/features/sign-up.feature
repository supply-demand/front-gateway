#Feature: User's sign-up
#
#  Scenario: Sign up successfully
#    When I save new user I see response
#    """
#    {
#      "userID": "6aaa21cf-6734-4b4e-a7a4-2740c01b6418",
#      "etag": "0000000000"
#    }
#    """
#    When I save new session I see "abc"
#    Then I send API request to POST "/auth/sign-up" with
#    """
#    {
#      "email": "abcdef@gmail.com",
#      "password": "hello"
#    }
#    """
#    Then I see API response status "201"
#    Then I see API response body
#    """
#    {
#      "session": "abc"
#    }
#    """
#
#  Scenario: Create new user and drop expired un-confirmed duplicated user
#    When I save new user I see error code "6"
#
#    When I look for user by email and password I see response
#    """
#    {
#      "user": {
#        "id": "6aaa21cf-6734-4b4e-a7a4-2740c01b6418",
#        "email": "foobar@gmail.com",
#        "name": "Foo",
#        "phone": "+12345678",
#        "passwordHash": "aBk2KlcA0sow",
#        "status": 0,
#        "createdAt": {
#          "seconds": 1635690671,
#          "nanos": 0
#        },
#        "etag": "0000000000"
#      }
#    }
#    """
#
#    When I check current time I see "2021-10-31T15:31:11+00:00"
#
#    When I remove user I see response
#    """
#    {
#      "etag": "0000000000"
#    }
#    """
#
#    When I save new user I see response
#    """
#    {
#      "userID": "7eb08e50-ee2a-4b36-b458-b0136d4dc4a5",
#      "etag": "0000000000"
#    }
#    """
#
#    When I save new session I see "cde"
#
#    Then I send API request to POST "/auth/sign-up" with
#    """
#    {
#      "email": "foobar@gmail.com",
#      "password": "hello"
#    }
#    """
#
#    Then I see API response status "201"
#
#    Then I see API response body
#    """
#    {
#      "session": "cde"
#    }
#    """
#
#  Scenario: Do not create new user in case of duplicate
#    When I save new user I see error code "6"
#
#    When I look for user by email and password I see response
#    """
#    {
#      "user": {
#        "id": "6aaa21cf-6734-4b4e-a7a4-2740c01b6418",
#        "email": "foobar@gmail.com",
#        "name": "Foo",
#        "phone": "+12345678",
#        "passwordHash": "aBk2KlcA0sow",
#        "status": 1,
#        "createdAt": {
#          "seconds": 1635690671,
#          "nanos": 0
#        },
#        "etag": "0000000000"
#      }
#    }
#    """
#
#    Then I send API request to POST "/auth/sign-up" with
#    """
#    {
#      "email": "foobar@gmail.com",
#      "password": "hello"
#    }
#    """
#
#    Then I see API response status "409"
#
#    Then I see API response body
#    """
#    {
#      "userAlreadyExists": null
#    }
#    """