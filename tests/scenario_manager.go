package main

import (
	"context"
	"net"
	"testing"

	"github.com/cucumber/godog"
	"github.com/golang/mock/gomock"
	"gitlab.com/supply-demand/auth/tests/contexts"
	"gitlab.com/supply-demand/auth/tests/mocks"
	"go.uber.org/fx"
)

type ScenarioManager struct {
	t      *testing.T
	socket *net.Listener
}

func NewScenarioManager(t *testing.T) *ScenarioManager {
	return &ScenarioManager{
		t: t,
	}
}

func (sm *ScenarioManager) initialize(sc *godog.ScenarioContext) {
	var scenarioApp *fx.App

	socket, apiAddress, err := pickTCPSocket()
	if err != nil {
		sm.t.Error(err)
		return
	}

	sm.socket = socket

	scenarioApp = fx.New(
		fx.NopLogger,
		fx.Supply(sm.t),
		fx.Supply(sc),
		fx.Supply(sm.socket),
		fx.Supply(contexts.TCPAddress(apiAddress)),
		fx.Provide(
			gomock.NewController,

			mocks.NewMockUserServiceClient,
			mocks.NewMockStorage,
			mocks.NewMockClock,

			contexts.NewUserServiceContext,
			contexts.NewSessionServiceContext,
			contexts.NewAPIContext,
			contexts.NewClockContext,

			func(t *testing.T) gomock.TestReporter {
				return t
			},

			NewTestingApp,
		),
		fx.Invoke(func(
			userServiceCtx *contexts.UserServiceContext,
			sessionServiceCtx *contexts.SessionServiceContext,
			apiCtx *contexts.APIContext,
			clockCtx *contexts.ClockContext,
			scenarioCtx *godog.ScenarioContext,
			lifecycle fx.Lifecycle,
			testingApp TestingApp,
		) {
			lifecycle.Append(fx.Hook{
				OnStart: func(ctx context.Context) error {
					return (*testingApp).Start(ctx)
				},
				OnStop: func(ctx context.Context) error {
					if err := (*testingApp).Stop(ctx); err != nil {
						return err
					}

					return (*testingApp).Err()
				},
			})

			userServiceCtx.Register(scenarioCtx)
			sessionServiceCtx.Register(scenarioCtx)
			apiCtx.Register(scenarioCtx)
			clockCtx.Register(scenarioCtx)
		}),
	)

	sc.After(func(ctx context.Context, sc *godog.Scenario, err error) (context.Context, error) {
		errStop := scenarioApp.Stop(ctx)

		if err != nil {
			return ctx, err
		}

		if errStop != nil {
			return ctx, errStop
		}

		return ctx, scenarioApp.Err()
	})

	err = scenarioApp.Start(context.Background())
	if err != nil {
		sm.t.Fatal("Could not initialize scenario", err)
	}
}
