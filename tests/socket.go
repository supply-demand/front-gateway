package main

import (
	"errors"
	"fmt"
	"net"
	"os"
	"syscall"
)

func pickSocketRecursively(port int, attempt int) (*net.Listener, string, error) {
	attempt++
	if attempt > 10 {
		return nil, "", errors.New("max attempts reached while picking free socket")
	}

	port++
	addr, err := net.ResolveTCPAddr("tcp4", fmt.Sprintf(":%d", port))
	if err != nil {
		return nil, "", err
	}

	socket, err := net.Listen("tcp4", addr.String())
	if isAddrInUse(err) {
		return pickSocketRecursively(port, attempt)
	}

	return &socket, addr.String(), nil
}

func pickTCPSocket() (*net.Listener, string, error) {
	port := 9000
	addr, err := net.ResolveTCPAddr("tcp4", fmt.Sprintf(":%d", port))
	if err != nil {
		return nil, "", err
	}
	socket, err := net.Listen("tcp4", addr.String())

	if isAddrInUse(err) {
		return pickSocketRecursively(port, 1)
	}

	return &socket, addr.String(), err
}

func isAddrInUse(e error) bool {
	if opErr, ok := e.(*net.OpError); ok {
		if syscallErr, ok := opErr.Err.(*os.SyscallError); ok {
			if syscallErrno, ok := syscallErr.Err.(syscall.Errno); ok {
				if syscallErrno == syscall.EADDRINUSE {
					return true
				}
			}
		}
	}

	return false
}
