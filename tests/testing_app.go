package main

import (
	"context"
	"gitlab.com/supply-demand/auth/dependency/clock"
	"net"

	"github.com/sirupsen/logrus"
	"github.com/valyala/fasthttp"
	"gitlab.com/supply-demand/auth/app/httpservice"
	"gitlab.com/supply-demand/auth/app/httpservice/services"
	"gitlab.com/supply-demand/auth/dependency/session"
	"gitlab.com/supply-demand/auth/domain"
	"gitlab.com/supply-demand/auth/tests/mocks"
	"go.uber.org/fx"

	apiHandlers "gitlab.com/supply-demand/auth/app/httpservice/handlers"
	userProto "gitlab.com/supply-demand/proto_gen/go/user"
)

type TestingApp *fx.App

func NewTestingApp(
	userServiceClientMock *mocks.MockUserServiceClient,
	storageMock *mocks.MockStorage,
	clockMock *mocks.MockClock,
	socket *net.Listener,
) TestingApp {
	var app *fx.App
	app = fx.New(
		fx.NopLogger,
		domain.Module,
		fx.Supply(socket),
		fx.Provide(func() *logrus.Logger {
			logger := logrus.New()
			logger.SetLevel(logrus.WarnLevel)
			return logger
		}),
		fx.Provide(func() userProto.UserServiceClient {
			return userServiceClientMock
		}),
		fx.Provide(func() session.Storage {
			return storageMock
		}),
		fx.Provide(func() clock.Clock {
			return clockMock
		}),
		fx.Provide(httpservice.NewErrorHandler),
		fx.Provide(httpservice.NewFastHttpHandler),
		fx.Provide(httpservice.NewFastHttpServer),
		fx.Provide(apiHandlers.NewAuthHandler),
		fx.Provide(services.NewAuthService),
		fx.Invoke(
			func(
				handler fasthttp.RequestHandler,
				socket *net.Listener,
				logger *logrus.Logger,
				lc fx.Lifecycle,
				fxShutDowner fx.Shutdowner,
			) error {
				var errSocket chan error

				httpServer := fasthttp.Server{
					Handler:            handler,
					CloseOnShutdown:    true,
					MaxRequestsPerConn: 1,
				}

				lc.Append(fx.Hook{
					OnStart: func(ctx context.Context) error {
						go func() {
							if err := httpServer.Serve(*socket); err != nil {
								errSocket <- err
								_ = fxShutDowner.Shutdown()
							}
						}()

						return nil
					},
					OnStop: func(ctx context.Context) error {
						select {
						case err := <-errSocket:
							return err
						default:
							go func() {
								err := httpServer.Shutdown()
								if err != nil {
									logger.Errorf("Could not shutdown http server: %s", err)
								}
							}()
						}

						return nil
					},
				})

				return nil
			},
		),
	)

	return app
}
